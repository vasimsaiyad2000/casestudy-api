package com.casestudy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.casestudy.dto.BaseDTO;
import com.casestudy.dto.LoginDTO;
import com.casestudy.enums.Status;
import com.casestudy.service.UserService;

/**
 * This class contains all the end points for user.
 * @author Vasim Saiyad
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * Authenticate user for given email and password
     * @param loginRequest
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public BaseDTO login(@RequestBody LoginDTO loginRequest) {
        // Validating login request
        loginRequest.validate();
        
        LoginDTO loginResponse = userService.login(loginRequest.getEmail(), loginRequest.getPassword());
        return new BaseDTO(Status.SUCCESS, loginResponse);
    }
}
