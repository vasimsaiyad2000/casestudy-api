package com.casestudy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.casestudy.dto.BaseDTO;
import com.casestudy.dto.CurrencyDTO;
import com.casestudy.enums.Status;
import com.casestudy.service.CurrencyService;

/**
 * This is contains all the end point for currency.
 * @author Vasim Saiyad
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/v1/currency")
public class CurrencyController {

    @Autowired
    private CurrencyService currencyService;
    
    /**
     * Create a new currency 
     * @param currencyRequest
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public BaseDTO create(@RequestBody CurrencyDTO currencyRequest) {
        
        // Validating request
        currencyRequest.validate();
        currencyService.create(currencyRequest);
        return new BaseDTO(Status.CREATED);
    }
    
    /**
     * Get all currencies in descending order by created date
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public BaseDTO list() {
        List<CurrencyDTO> list = currencyService.list();
        return new BaseDTO(Status.SUCCESS, list);
    }
}
