package com.casestudy.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Vasim Saiyad
 *
 */
@Entity
@Table(name = "currency")
public class Currency implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -433031285936374716L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DEX_ROW_ID")
    private Integer id;

    @Column(name = "CURNCYID")
    private String currencyId;

    @Column(name = "CURRNIDX")
    private Integer currencyIndex;

    @Column(name = "CRNCYDSC")
    private String currencyDesc;

    @Column(name = "CRNCYDSCA")
    private String currencyDescArabic;

    @Column(name = "CRNCYSYM")
    private String currencySymbol;

    @Column(name = "INCLSPAC")
    private Boolean isIncludeSpace;

    @Column(name = "NEGSYMBL")
    private Integer negativeSymbol;

    @Column(name = "NGSMAMPC")
    private Integer displaySymbol;

    @Column(name = "DECSYMBL")
    private Integer decimalSymbol;

    @Column(name = "THOUSSYM")
    private Integer thousandSymbol;

    @Column(name = "CURTEXT_1")
    private String currencyUnit;

    @Column(name = "CURTEXT_2")
    private String subUnitConnector;

    @Column(name = "CURTEXT_3")
    private String currencySubUnit;

    @Column(name = "CURTEXTA_1")
    private String currencyUnitArabic;

    @Column(name = "CURTEXTA_2")
    private String subUnitConnectorArabic;

    @Column(name = "CURTEXTA_3")
    private String currencySubUnitArabic;

    @Column(name = "DEX_ROW_TS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Integer getCurrencyIndex() {
        return currencyIndex;
    }

    public void setCurrencyIndex(Integer currencyIndex) {
        this.currencyIndex = currencyIndex;
    }

    public String getCurrencyDesc() {
        return currencyDesc;
    }

    public void setCurrencyDesc(String currencyDesc) {
        this.currencyDesc = currencyDesc;
    }

    public String getCurrencyDescArabic() {
        return currencyDescArabic;
    }

    public void setCurrencyDescArabic(String currencyDescArabic) {
        this.currencyDescArabic = currencyDescArabic;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public Boolean getIsIncludeSpace() {
        return isIncludeSpace;
    }

    public void setIsIncludeSpace(Boolean isIncludeSpace) {
        this.isIncludeSpace = isIncludeSpace;
    }

    public Integer getNegativeSymbol() {
        return negativeSymbol;
    }

    public void setNegativeSymbol(Integer negativeSymbol) {
        this.negativeSymbol = negativeSymbol;
    }

    public Integer getDisplaySymbol() {
        return displaySymbol;
    }

    public void setDisplaySymbol(Integer displaySymbol) {
        this.displaySymbol = displaySymbol;
    }

    public Integer getDecimalSymbol() {
        return decimalSymbol;
    }

    public void setDecimalSymbol(Integer decimalSymbol) {
        this.decimalSymbol = decimalSymbol;
    }

    public Integer getThousandSymbol() {
        return thousandSymbol;
    }

    public void setThousandSymbol(Integer thousandSymbol) {
        this.thousandSymbol = thousandSymbol;
    }

    public String getCurrencyUnit() {
        return currencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    public String getSubUnitConnector() {
        return subUnitConnector;
    }

    public void setSubUnitConnector(String subUnitConnector) {
        this.subUnitConnector = subUnitConnector;
    }

    public String getCurrencySubUnit() {
        return currencySubUnit;
    }

    public void setCurrencySubUnit(String currencySubUnit) {
        this.currencySubUnit = currencySubUnit;
    }

    public String getCurrencyUnitArabic() {
        return currencyUnitArabic;
    }

    public void setCurrencyUnitArabic(String currencyUnitArabic) {
        this.currencyUnitArabic = currencyUnitArabic;
    }

    public String getSubUnitConnectorArabic() {
        return subUnitConnectorArabic;
    }

    public void setSubUnitConnectorArabic(String subUnitConnectorArabic) {
        this.subUnitConnectorArabic = subUnitConnectorArabic;
    }

    public String getCurrencySubUnitArabic() {
        return currencySubUnitArabic;
    }

    public void setCurrencySubUnitArabic(String currencySubUnitArabic) {
        this.currencySubUnitArabic = currencySubUnitArabic;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
