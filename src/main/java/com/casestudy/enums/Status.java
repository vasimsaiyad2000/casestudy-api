package com.casestudy.enums;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public enum Status {

    SUCCESS, CREATED;
}
