package com.casestudy.enums;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public enum ErrorCode {

    // USER ERROR CODE - 1xx
    EMAIL_REQUIRED(101, "Email is mandatory field"),
    PASSWORD_REQUIRED(102, "Password is mandatory field"),
    INVALID_LOGIN(103, "Invalid email and/or password"),
    
    // CURRENCY ERROR CODE - 2xx
    CURRENCY_ID_REQUIRED(201, "Currency Id is mandatory field"),
    CURRENCY_DESC_REQUIRED(202, "Description is mandatory field"),
    CURRENCY_ARABIC_DESC_REQUIRED(203, "Arabic Description is mandatory field"),
    CURRENCY_SYMBOL_REQUIRED(204, "Currency Symbol is mandatory field"),
    CURRENCY_SYMBOL_INVALID(205, "Currency Symbol must be upto 3 character"),
    CURRENCY_ID_CONFLICT(206, "Currency Id is already register"),
    NEGATIVE_SIGN_REQUIRE(207, "Negative Sign is mandatory field"),
    DISPLAY_SYMBOL_REQUIRE(208, "Please select display symbol"),
    DESIMAL_SEPARATOR_REQUIRE(208, "Please select decimal separator"),
    THOUSAND_SEPARATOR_REQUIRE(208, "Please select thousand separator");
    
    private Integer code;
    private String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
