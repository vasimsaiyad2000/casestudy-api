package com.casestudy.service;

import java.util.List;
import com.casestudy.dto.CurrencyDTO;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public interface CurrencyService {

    /**
     * Create a new currency
     * @param currencyDTO Given currency details
     * @return
     */
    public void create(CurrencyDTO currencyDTO);
    
    /**
     * Get all currencies 
     * @return
     */
    public List<CurrencyDTO> list();
}
