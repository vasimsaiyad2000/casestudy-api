package com.casestudy.service;

import com.casestudy.dto.LoginDTO;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public interface UserService {

    /**
     * Authenticate User for given credentials
     * @param email Given email ID
     * @param password Given password
     * @return
     */
    public LoginDTO login(String email, String password);
}
