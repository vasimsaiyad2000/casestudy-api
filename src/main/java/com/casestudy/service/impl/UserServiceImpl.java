package com.casestudy.service.impl;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.dto.LoginDTO;
import com.casestudy.enums.ErrorCode;
import com.casestudy.exceptions.BadRequestException;
import com.casestudy.model.User;
import com.casestudy.repository.UserRepository;
import com.casestudy.service.UserService;

/**
 * 
 * @author Vasim Saiyad
 *
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private Mapper mapper;
    
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public LoginDTO login(String email, String password) {
        User user = userRepository.findByEmailAndPassword(email, password);
        
        if (user == null) {
            throw new BadRequestException(ErrorCode.INVALID_LOGIN);
        }
        
        user.setPassword(null);
        return mapper.map(user, LoginDTO.class);
    }
}
