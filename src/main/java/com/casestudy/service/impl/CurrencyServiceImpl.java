package com.casestudy.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.dto.CurrencyDTO;
import com.casestudy.enums.ErrorCode;
import com.casestudy.exceptions.BadRequestException;
import com.casestudy.model.Currency;
import com.casestudy.repository.CurrencyRepository;
import com.casestudy.service.CurrencyService;

/**
 * 
 * @author Vasim Saiyad
 *
 */

@Service
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    private Mapper mapper;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Override
    @Transactional
    public void create(CurrencyDTO currencyDTO) {
        Currency currency = currencyRepository.findByCurrencyId(currencyDTO.getCurrencyId());

        if (currency != null) {
            throw new BadRequestException(ErrorCode.CURRENCY_ID_CONFLICT);
        }
        
        currency = new Currency();
        
        mapper.map(currencyDTO, currency);
        currency.setCreatedDate(new Date());
        
        // Getting max currency index
        Long currencyIndex = currencyRepository.getMaxCurrencyIndex();
        currency.setCurrencyIndex(currencyIndex.intValue() + 1);
        
        currencyRepository.save(currency);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CurrencyDTO> list() {
        Pageable pageable = new PageRequest(0, 10, Direction.DESC , "createdDate");
        Page<Currency> currencies = currencyRepository.findAll(pageable);
        
        List<CurrencyDTO> currencyList = new ArrayList<>();
        for (Currency currency : currencies.getContent()) {
            CurrencyDTO currencyDTO = new CurrencyDTO();
            mapper.map(currency, currencyDTO);
            
            currencyList.add(currencyDTO);
        }
        
        return currencyList;
    }
}
