package com.casestudy.config;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * 
 * @author Vasim Saiyad
 *
 */
@Configuration
public class SwaggerConfiguration {

    @Bean
    public Docket fdaApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("test api")
                .select()
                .paths(regex("/api/v1/.*"))
                .build();
    }
}
