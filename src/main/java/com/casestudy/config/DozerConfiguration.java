package com.casestudy.config;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author Vasim Saiyad
 *
 */
@Configuration
public class DozerConfiguration {

    @Bean
    public Mapper mapper() {
        return new DozerBeanMapper();
    }
}
