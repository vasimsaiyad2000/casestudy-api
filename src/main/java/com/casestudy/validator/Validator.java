package com.casestudy.validator;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public interface Validator {

    public void validate();
}
