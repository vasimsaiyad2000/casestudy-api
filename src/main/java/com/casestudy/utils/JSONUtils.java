package com.casestudy.utils;

import java.io.File;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.casestudy.exceptions.AppException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Vasim Saiyad
 *
 */

public class JSONUtils {

    private static Logger logger = LoggerFactory.getLogger(JSONUtils.class);

    private JSONUtils() {
        // Do nothing
    }

    public static <T> T parse(String filePath, Class<T> clazz) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(getResourceFile(filePath), clazz);
        } catch (Exception e) {
            logger.error("There is an issue parsing json for file path :" + filePath, e.getMessage());
            throw new AppException("There is an issue parsing json for file path :" + filePath, e);
        }
    }

    public static <T> T parseJson(String json, Class<T> clazz) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            logger.error("There is an issue parsing json :" + json, e.getMessage());
            throw new AppException("There is an issue parsing json :" + json, e);
        }
    }

    public static <T> List<T> parseList(String filePath, Class<T> clazz) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
            return mapper.readValue(getResourceFile(filePath), type);
        } catch (Exception e) {
            logger.error("There is an issue parsing json for file path :" + filePath, e.getMessage());
            throw new AppException("There is an issue parsing json for file path :" + filePath, e);
        }
    }

    public static String toJsonString(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            logger.error("There is an issue converting json to string :" + object, e.getMessage());
            throw new AppException("There is an issue converting json to string :" + object, e);
        }
    }

    public static String toJsonPrettyString(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            logger.error("There is an issue converting json to pretty string :" + object, e.getMessage());
            throw new AppException("There is an issue converting json to pretty string :" + object, e);
        }
    }

    private static File getResourceFile(String filePath) {
        ClassLoader classLoader = JSONUtils.class.getClassLoader();
        return new File(classLoader.getResource(filePath).getFile());
    }
}
