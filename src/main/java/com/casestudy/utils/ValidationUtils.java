package com.casestudy.utils;

import org.apache.commons.lang3.StringUtils;
import com.casestudy.enums.ErrorCode;
import com.casestudy.exceptions.BadRequestException;

/**
 * This class perform validations
 *
 * @author Vasim Saiyad
 */
public class ValidationUtils {

    public static <T> T checkNonNull(T obj, ErrorCode error) {
        if (isNullOrEmpty(obj)) {
            throw new BadRequestException(error);
        }

        return obj;
    }
    
    public static <T> boolean isNullOrEmpty(T obj) {
        if (obj == null) {
            return true;
        }

        if (obj.getClass() == String.class) {
            String value = (String) obj;
            if (StringUtils.isBlank(value)) {
                return true;
            }
        }

        return false;
    }
    
    public static void checkValidRange(int min, int max, int value, ErrorCode error) {
        if (value < min || value > max) {
            throw new BadRequestException(error);
        }
    }

}
