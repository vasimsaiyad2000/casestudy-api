package com.casestudy.dto;

import com.casestudy.enums.ErrorCode;
import com.casestudy.utils.ValidationUtils;
import com.casestudy.validator.Validator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author Vasim Saiyad
 *
 */
@JsonInclude(value = Include.NON_NULL)
public class CurrencyDTO implements Validator {

    private String currencyId;
    private String currencyDesc;
    private String currencyDescArabic;
    private String currencySymbol;
    private Boolean isIncludeSpace;
    private Integer negativeSymbol;
    private Integer displaySymbol;
    private Integer decimalSymbol;
    private Integer thousandSymbol;
    private String currencyUnit;
    private String subUnitConnector;
    private String currencySubUnit;
    private String currencyUnitArabic;
    private String subUnitConnectorArabic;
    private String currencySubUnitArabic;

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getCurrencyDesc() {
        return currencyDesc;
    }

    public void setCurrencyDesc(String currencyDesc) {
        this.currencyDesc = currencyDesc;
    }

    public String getCurrencyDescArabic() {
        return currencyDescArabic;
    }

    public void setCurrencyDescArabic(String currencyDescArabic) {
        this.currencyDescArabic = currencyDescArabic;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public Boolean getIsIncludeSpace() {
        return isIncludeSpace;
    }

    public void setIsIncludeSpace(Boolean isIncludeSpace) {
        this.isIncludeSpace = isIncludeSpace;
    }

    public Integer getNegativeSymbol() {
        return negativeSymbol;
    }

    public void setNegativeSymbol(Integer negativeSymbol) {
        this.negativeSymbol = negativeSymbol;
    }

    public Integer getDisplaySymbol() {
        return displaySymbol;
    }

    public void setDisplaySymbol(Integer displaySymbol) {
        this.displaySymbol = displaySymbol;
    }

    public Integer getDecimalSymbol() {
        return decimalSymbol;
    }

    public void setDecimalSymbol(Integer decimalSymbol) {
        this.decimalSymbol = decimalSymbol;
    }

    public Integer getThousandSymbol() {
        return thousandSymbol;
    }

    public void setThousandSymbol(Integer thousandSymbol) {
        this.thousandSymbol = thousandSymbol;
    }

    public String getCurrencyUnit() {
        return currencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    public String getSubUnitConnector() {
        return subUnitConnector;
    }

    public void setSubUnitConnector(String subUnitConnector) {
        this.subUnitConnector = subUnitConnector;
    }

    public String getCurrencySubUnit() {
        return currencySubUnit;
    }

    public void setCurrencySubUnit(String currencySubUnit) {
        this.currencySubUnit = currencySubUnit;
    }

    public String getCurrencyUnitArabic() {
        return currencyUnitArabic;
    }

    public void setCurrencyUnitArabic(String currencyUnitArabic) {
        this.currencyUnitArabic = currencyUnitArabic;
    }

    public String getSubUnitConnectorArabic() {
        return subUnitConnectorArabic;
    }

    public void setSubUnitConnectorArabic(String subUnitConnectorArabic) {
        this.subUnitConnectorArabic = subUnitConnectorArabic;
    }

    public String getCurrencySubUnitArabic() {
        return currencySubUnitArabic;
    }

    public void setCurrencySubUnitArabic(String currencySubUnitArabic) {
        this.currencySubUnitArabic = currencySubUnitArabic;
    }

    @Override
    public void validate() {

        ValidationUtils.checkNonNull(this.currencyId, ErrorCode.CURRENCY_ID_REQUIRED);
        ValidationUtils.checkNonNull(this.currencyDesc, ErrorCode.CURRENCY_DESC_REQUIRED);
        ValidationUtils.checkNonNull(this.currencyDescArabic, ErrorCode.CURRENCY_ARABIC_DESC_REQUIRED);
        ValidationUtils.checkNonNull(this.currencySymbol, ErrorCode.CURRENCY_SYMBOL_REQUIRED);
        ValidationUtils.checkValidRange(1, 3, this.currencySymbol.length(), ErrorCode.CURRENCY_SYMBOL_INVALID);
        ValidationUtils.checkNonNull(this.negativeSymbol, ErrorCode.NEGATIVE_SIGN_REQUIRE);
        ValidationUtils.checkNonNull(this.displaySymbol, ErrorCode.DISPLAY_SYMBOL_REQUIRE);
        ValidationUtils.checkNonNull(this.decimalSymbol, ErrorCode.DESIMAL_SEPARATOR_REQUIRE);
        ValidationUtils.checkNonNull(this.thousandSymbol, ErrorCode.THOUSAND_SEPARATOR_REQUIRE);
        
    }
}
