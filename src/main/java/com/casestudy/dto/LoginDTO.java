package com.casestudy.dto;

import com.casestudy.enums.ErrorCode;
import com.casestudy.utils.ValidationUtils;
import com.casestudy.validator.Validator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author Vasim Saiyad
 *
 */
@JsonInclude(value = Include.NON_NULL)
public class LoginDTO implements Validator {

    private String email;
    private String password;
    private String firstName;
    private String lastName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public void validate() {
        ValidationUtils.checkNonNull(this.email, ErrorCode.EMAIL_REQUIRED);
        ValidationUtils.checkNonNull(this.password, ErrorCode.PASSWORD_REQUIRED);
    }
}
