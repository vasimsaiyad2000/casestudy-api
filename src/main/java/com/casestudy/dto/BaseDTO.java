package com.casestudy.dto;

import com.casestudy.enums.Status;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author Vasim Saiyad
 *
 */
@JsonInclude(value = Include.NON_NULL)
public class BaseDTO {

    private Status status;
    private Object data;

    public BaseDTO() {
        // Do nothing
    }

    public BaseDTO(Status status) {
        this.status = status;
    }

    public BaseDTO(Status status, Object data) {
        this.status = status;
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
