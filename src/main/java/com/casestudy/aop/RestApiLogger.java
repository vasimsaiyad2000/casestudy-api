package com.casestudy.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Vasim Saiyad
 *
 */
@Component
@Aspect
public class RestApiLogger {

    private static final String EXCEPTION = "We have caught exception in method :  %s \n Exception is : %s";
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private HttpServletRequest request;

    @Before("execution(* com.casestudy.controller.*.*(..))")
    public void logEntry(JoinPoint joinPoint) {
        logger.debug("Service end point : " + request.getRequestURI());
        logger.debug("Service http method : " + request.getMethod());
        logger.debug("Service class : " + joinPoint.getTarget().getClass().getName());
        logger.debug("Service method : " + joinPoint.getSignature().getName());
    }

    @AfterReturning(pointcut = "execution(* com.casestudy.controller.*.*(..))", returning = "result")
    public void logResponse(JoinPoint joinPoint, Object result) {
        logger.debug("Service Response Joint point :", joinPoint.toString());
        logger.debug("Service Response :", result);
    }

    @AfterThrowing(pointcut = "execution(* com.casestudy.controller.*.*(..))", throwing = "e")
    public void logControllerException(JoinPoint joinPoint, Exception e) {
        logger.error(String.format(EXCEPTION, joinPoint.getSignature().getName(), e.getMessage()), e);
    }

    @AfterThrowing(pointcut = "execution(* com.casestudy.service.impl.*.*(..))", throwing = "e")
    public void logServiceException(JoinPoint joinPoint, Exception e) {
        logger.error(String.format(EXCEPTION, joinPoint.getSignature().getName(), e.getMessage()), e);
    }
}
