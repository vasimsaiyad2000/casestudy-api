package com.casestudy.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Vasim Saiyad This class handle all exceptions thrown by any
 * controller method at the same place.
 * <p>
 * It allows to apply exception handling at one place instead of applying in
 * every controller method.
 * <p>
 * It handle below exceptions and provide appropriate response for exception to
 * client.
 */
@ControllerAdvice
public class AbstractExceptionHandler {

    /**
     * This method handle exception for bad request and throw the exception to
     * api.
     *
     * @param ex
     *            BadRequestException
     * @return Error message with HttpStatus code 400
     * @throws BadRequestException
     */
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResource handleBadRequestException(BadRequestException ex) {
        return new ErrorResource(ex.getCode(), ex.getMessage());
    }
}
