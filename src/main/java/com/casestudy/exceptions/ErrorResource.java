package com.casestudy.exceptions;

import java.io.Serializable;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public class ErrorResource implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8338007951155024302L;
    private Integer code;
    private String detail;

    public ErrorResource() {
        // Do nothing
    }

    public ErrorResource(Integer code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
