package com.casestudy.exceptions;

import com.casestudy.enums.ErrorCode;

/**
 * This is a base exception class of the application that inherit
 * RuntimeException class.
 * <p>
 * This class will be inherited by other exception classes to throws exception.
 *
 * @author vasim
 */
public class AppException extends RuntimeException {

    private Integer code;

    public AppException() {
        super();
    }

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(ErrorCode error, Object ... object) {
        super(String.format(error.getMessage(), object));
        this.code = error.getCode();
    }

    public Integer getCode() {
        return code;
    }
}