package com.casestudy.exceptions;

import com.casestudy.enums.ErrorCode;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public class ConflictException extends AppException {

    /**
     * Default constructor
     */
    private static final long serialVersionUID = -2186626604176573521L;

    public ConflictException() {
        // Do nothing
        super();
    }

    /**
     * Parameterized constructor
     *
     * @param message
     *            exception message
     */
    public ConflictException(String message) {
        super(message);
    }

    /**
     * Handle Multiple Exception message
     *
     * @param message
     * @param values
     */
    public ConflictException(String message, Object... values) {
        super(String.format(message, values));
    }

   /**
    * Parameterized constructor
    * @param error
    * @param object
    */
    public ConflictException(ErrorCode error, Object... object) {
        super(error, object);
    }
}
