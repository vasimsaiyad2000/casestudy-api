package com.casestudy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.casestudy.model.User;
/**
 * 
 * @author Vasim Saiyad
 *
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    public User findByEmailAndPassword(String email, String password);
}
