package com.casestudy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.casestudy.model.Currency;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public interface CurrencyRepository extends JpaRepository<Currency, Integer> {

    public Currency findByCurrencyId(String currencyId);
    
    @Query("SELECT coalesce(max(c.currencyIndex), 0) FROM Currency c")
    public Long getMaxCurrencyIndex();
}
