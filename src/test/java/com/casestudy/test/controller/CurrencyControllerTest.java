package com.casestudy.test.controller;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.casestudy.controller.CurrencyController;
import com.casestudy.dto.CurrencyDTO;
import com.casestudy.enums.ErrorCode;
import com.casestudy.exceptions.AbstractExceptionHandler;
import com.casestudy.exceptions.BadRequestException;
import com.casestudy.service.CurrencyService;
import com.casestudy.test.utils.AbstractBaseTest;
import com.casestudy.test.utils.TestUtils;
import com.casestudy.utils.JSONUtils;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public class CurrencyControllerTest extends AbstractBaseTest {

    @InjectMocks
    private CurrencyController currencyController;

    @Mock
    private CurrencyService currencyService;

    @Mock
    private CurrencyDTO currencyDTOMock;
    
    @Before
    public void setUp() {
        super.setUp();
        mockMvc = MockMvcBuilders.standaloneSetup(currencyController)
                .setControllerAdvice(new AbstractExceptionHandler())
                .build();
    }
    
    @Test
    public void get_currencies_OK() throws Exception {
        List<CurrencyDTO> currencies = JSONUtils.parseList("currencyList.json", CurrencyDTO.class);
        when(currencyService.list()).thenReturn(currencies);

        mockMvc.perform(get("/api/v1/currency"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andDo(print());

        verify(currencyService, times(1)).list();
    }

    @Test
    public void create_currency_OK() throws Exception {
        CurrencyDTO currency = JSONUtils.parse("currency.json", CurrencyDTO.class);
   
        currencyDTOMock.validate();
        currencyService.create(currency);
        
        mockMvc.perform(post("/api/v1/currency")
                .contentType(TestUtils.APPLICATION_JSON_UTF8)
                .content(TestUtils.convertObjectToJsonBytes(currency)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andDo(print());

        verify(currencyService, times(1)).create(currency);
    }
    
    @Test
    public void create_currency_BadRequest() throws Exception {
        CurrencyDTO currency = JSONUtils.parse("currency.json", CurrencyDTO.class);
        currency.setCurrencyId("");
        
        doThrow(new BadRequestException(ErrorCode.CURRENCY_ID_REQUIRED)).when(currencyDTOMock).validate();
        mockMvc.perform(post("/api/v1/currency/")
                .contentType(TestUtils.APPLICATION_JSON_UTF8)
                .content(TestUtils.convertObjectToJsonBytes(currency)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andDo(print());
    }
}
