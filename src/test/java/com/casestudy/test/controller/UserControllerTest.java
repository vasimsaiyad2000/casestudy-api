package com.casestudy.test.controller;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.casestudy.controller.UserController;
import com.casestudy.dto.LoginDTO;
import com.casestudy.enums.ErrorCode;
import com.casestudy.exceptions.AbstractExceptionHandler;
import com.casestudy.exceptions.BadRequestException;
import com.casestudy.service.UserService;
import com.casestudy.test.utils.AbstractBaseTest;
import com.casestudy.test.utils.TestUtils;

/**
 * This class is responsible to test all test cases for user api.
 * 
 * @author Vasim Saiyad
 *
 */
public class UserControllerTest extends AbstractBaseTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Mock
    private LoginDTO loginDTOMock;

    @Before
    public void setUp() {
        super.setUp();
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice(new AbstractExceptionHandler())
                .build();
    }

    @Test
    public void login_OK() throws Exception {
        LoginDTO loginDTO = new LoginDTO();

        loginDTO.setEmail("test@test.com");
        loginDTO.setPassword("test");
        loginDTO.setFirstName("Test");
        loginDTO.setLastName("User");

        when(userService.login(loginDTO.getEmail(), loginDTO.getPassword())).thenReturn(loginDTO);

        mockMvc.perform(post("/api/v1/user/login")
                .contentType(TestUtils.APPLICATION_JSON_UTF8)
                .content(TestUtils.convertObjectToJsonBytes(loginDTO)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andDo(print());

        verify(userService, times(1)).login(loginDTO.getEmail(), loginDTO.getPassword());
    }

    @Test
    public void login_email_BadRequest() throws Exception {
        LoginDTO loginDTO = new LoginDTO();

        loginDTO.setEmail("");
        loginDTO.setPassword("test");

        doThrow(new BadRequestException(ErrorCode.EMAIL_REQUIRED)).when(loginDTOMock).validate();
        mockMvc.perform(post("/api/v1/user/login")
                .contentType(TestUtils.APPLICATION_JSON_UTF8)
                .content(TestUtils.convertObjectToJsonBytes(loginDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andDo(print());
    }

    @Test
    public void login_password_BadRequest() throws Exception {
        LoginDTO loginDTO = new LoginDTO();

        loginDTO.setEmail("test@test.com");
        loginDTO.setPassword("");

        doThrow(new BadRequestException(ErrorCode.PASSWORD_REQUIRED)).when(loginDTOMock).validate();
        mockMvc.perform(post("/api/v1/user/login")
                .contentType(TestUtils.APPLICATION_JSON_UTF8)
                .content(TestUtils.convertObjectToJsonBytes(loginDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andDo(print());
    }
    
    @Test
    public void login_invalid() throws Exception {
        LoginDTO loginDTO = new LoginDTO();

        loginDTO.setEmail("test@test.com");
        loginDTO.setPassword("test");

        when(userService.login(loginDTO.getEmail(), loginDTO.getPassword()))
                .thenThrow(new BadRequestException(ErrorCode.INVALID_LOGIN));
        mockMvc.perform(post("/api/v1/user/login")
                .contentType(TestUtils.APPLICATION_JSON_UTF8)
                .content(TestUtils.convertObjectToJsonBytes(loginDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andDo(print());
        
        verify(userService, times(1)).login(loginDTO.getEmail(), loginDTO.getPassword());
    }
}
