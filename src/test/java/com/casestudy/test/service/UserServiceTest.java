package com.casestudy.test.service;

import static org.mockito.Mockito.when;
import java.util.Date;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import com.casestudy.dto.LoginDTO;
import com.casestudy.exceptions.BadRequestException;
import com.casestudy.model.User;
import com.casestudy.repository.UserRepository;
import com.casestudy.service.impl.UserServiceImpl;
import com.casestudy.test.utils.AbstractBaseTest;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public class UserServiceTest extends AbstractBaseTest {

    @InjectMocks
    private UserServiceImpl userService;
    
    @Mock
    private UserRepository userRepository;
    
    @Mock
    private Mapper mapper;
    
    @Before
    public void setUp() {
        super.setUp();
    }
    
    @Test
    public void loginOKTest() {
        User user = mockUser();
        when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword())).thenReturn(user);
        when(mapper.map(user, LoginDTO.class)).thenReturn(mockLoginDTO());
        userService.login(user.getEmail(), user.getPassword());
    }
    
    @Test(expected = BadRequestException.class)
    public void loginFailedTest() {
        User user = mockUser();
        when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword())).thenReturn(null);
        userService.login(user.getEmail(), user.getPassword());
    }
    
    private User mockUser() {
        User user  = new User();
        user.setFirstName("Test");
        user.setLastName("User");
        user.setEmail("test@test.com");
        user.setPassword("test");
        user.setCreatedDate(new Date());
        
        return user;
    }
    
    private LoginDTO mockLoginDTO() {
        LoginDTO loginDTO  = new LoginDTO();
        loginDTO.setFirstName("Test");
        loginDTO.setLastName("User");
        loginDTO.setEmail("test@test.com");
        loginDTO.setPassword("test");
        
        return loginDTO;
    }
}
