package com.casestudy.test.service;

import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import com.casestudy.dto.CurrencyDTO;
import com.casestudy.model.Currency;
import com.casestudy.repository.CurrencyRepository;
import com.casestudy.service.impl.CurrencyServiceImpl;
import com.casestudy.test.utils.AbstractBaseTest;
import com.casestudy.utils.JSONUtils;

/**
 * 
 * @author Vasim Saiyad
 *
 */
public class CurrencyServiceTest extends AbstractBaseTest {

    @InjectMocks
    private CurrencyServiceImpl currencyService;

    @Mock
    private CurrencyRepository currencyRepository;

    @Mock
    private Mapper mapper;

    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void createOKTest() {
        CurrencyDTO currencyDTO = JSONUtils.parse("currency.json", CurrencyDTO.class);
        Currency currency = new Currency();
        mapper.map(currencyDTO, currency);

        currency.setCreatedDate(new Date());
        currencyRepository.save(currency);
        currencyService.create(currencyDTO);
    }
    
    @Test
    public void getCurrenciesOK() {
        List<Currency> currencies = JSONUtils.parseList("currencyList.json", Currency.class);
        Pageable pageable = new PageRequest(0, 10, Direction.DESC , "createdDate");
        
        Page<Currency> page = new PageImpl<Currency>(currencies);
        when(currencyRepository.findAll(pageable)).thenReturn(page);
        currencyService.list();
    }
}
